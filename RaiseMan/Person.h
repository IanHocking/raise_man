//
//  Person.h
//  RaiseMan
//
//  Created by ian.user on 25/09/2020.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface Person : NSObject {
    NSString *personName;
    float expectedRaise;
}

@property (readwrite, copy) NSString *personName;
@property (readwrite) float expectedRaise;

@end


NS_ASSUME_NONNULL_END
