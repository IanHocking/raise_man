//
//  Person.m
//  RaiseMan
//
//  Created by ian.user on 25/09/2020.
//

#import "Person.h"

@implementation Person

@synthesize personName;
@synthesize expectedRaise;

- (id)init
{
    self = [super init];
    if (self) {
        expectedRaise = 0.05;
        personName = @"New Person";
    }
    return self;
}

// expectedRaise cannot be passed nil because
// it needs to be a float. We will supply a default
// value that will be applied in the end of a nil
// being passed
- (void)setNilValueForKey:(NSString *)key
{

    if ([key isEqual:@"expectedRaise"]) // setting expectedRaise
    {
        [self setExpectedRaise:0.0]; // default
    } else { // setting anything else (nil allowed)
        [super setNilValueForKey:key];
    }

}

@end
